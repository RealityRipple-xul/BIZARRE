# BIZARRE
##### Basic Image Zoom And Rotate Rendering Extension

If you have ever had an image that was too large to fit on your screen or too small to see the finer detail in your browser, then BIZARRE can help. The Basic Image Zoom And Rotate Rendering Extension gives you complete control of the size of most images displayed in Pale Moon. Both individual images or whole pages of images can be zoomed.

#### Supports
 * Pale Moon [33.0 - 33.*]

## Building
Simply download the contents of the repository and pack the src folder into a .zip file. Then, rename the file to .xpi and drag into the browser.

## Download
You can grab the latest release from the [Official Web Site](//realityripple.com/Software/XUL/BIZARRE/).
