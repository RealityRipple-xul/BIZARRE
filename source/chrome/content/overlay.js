/* ***** BEGIN LICENSE BLOCK *****

 Copyright (c) 2006-2013  Jason Adams <imagezoom@yellowgorilla.net>
 Pale Moon Update:
               2020-2021  Andrew Sachen <webmaster@RealityRipple.com>

 This file is part of Image Zoom (now BIZARRE).

 Image Zoom is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Image Zoom is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Image Zoom; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 * ***** END LICENSE BLOCK ***** */

var bizarre =
{
 _Prefs: Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.bizarre.'),
 _context: null,
 _contextDisabled: false,
 _contextSubMenuLabel: null,
 _contextRotateMenuLabel: null,
 rotateTime: 0,
 _tmpImg: null,
 _linuxImg: null,
 _curImg: null,
 _scrollZooming: false,
 _haveZoomed: false,
 init: function()
 {
  if (document.getElementById('contentAreaContextMenu'))
   document.getElementById('contentAreaContextMenu').addEventListener('popupshowing', bizarre.showMenu, false);
  window.addEventListener('mousedown', bizarre.izOnMouseDown, true);
 },
 izShowCustomZoom: function()
 {
  var oizImage = new zarImage(document.popupNode);
  openDialog('chrome://bizarre/content/customzoom.xul', '', 'chrome,modal,centerscreen', 'Image', oizImage);
  bizarre._reportStatus(oizImage);
 },
 izShowCustomDim: function()
 {
  var oizImage = new zarImage(document.popupNode);
  openDialog('chrome://bizarre/content/customdim.xul', '', 'chrome,modal,centerscreen', oizImage);
  bizarre._reportStatus(oizImage);
 },
 izImageFit: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.fit(bizarre._Prefs.getBoolPref('autocenter'));
  bizarre._reportStatus(oizImage);
 },
 izFitWidth: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.fit(bizarre._Prefs.getBoolPref('autocenter'), true);
  bizarre._reportStatus(oizImage);
 },
 izZoomIn: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.zoom(bizarre._Prefs.getIntPref('zoomvalue') / 100);
  bizarre._reportStatus(oizImage);
 },
 izZoomOut: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.zoom(100 / bizarre._Prefs.getIntPref('zoomvalue'));
  bizarre._reportStatus(oizImage);
 },
 izSetZoom: function(zFactor)
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.setZoom(zFactor);
  bizarre._reportStatus(oizImage);
 },
 izRotateRight: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.rotate(90);
  bizarre._tmpImg = oizImage;
 },
 izRotateLeft: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.rotate(-90);
  bizarre._tmpImg = oizImage;
 },
 izRotate180: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.rotate(180);
  bizarre._tmpImg = oizImage;
 },
 izFlipH: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.rotate(undefined, 'h');
  bizarre._tmpImg = oizImage;
 },
 izFlipV: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.rotate(undefined, 'v');
  bizarre._tmpImg = oizImage;
 },
 izRotateReset: function()
 {
  var oizImage = new zarImage(document.popupNode);
  oizImage.resetFlip();
  oizImage.rotate(0 - oizImage.getAngle());
  bizarre._tmpImg = oizImage;
 },
 _getContextSubMenuLabel: function()
 {
  if (bizarre._contextSubMenuLabel === null)
   bizarre._contextSubMenuLabel = document.getElementById('context-zoomsub').getAttribute('label') + ' (%zoom% %)';
  return bizarre._contextSubMenuLabel;
 },
 _getContextRotateMenuLabel: function()
 {
  if (bizarre._contextRotateMenuLabel === null)
    bizarre._contextRotateMenuLabel = document.getElementById('context-rotatesub').getAttribute('label') + ' (%rotate%\u00B0)';
  return bizarre._contextRotateMenuLabel;
 },
 _cancelScrollZoom: function()
 {
  bizarre._linuxImg = null;
  bizarre._curImg = null;
  bizarre._scrollZooming = false;
  window.removeEventListener('wheel', bizarre.scrollImage, true);
  window.removeEventListener('mouseup', bizarre.izOnMouseUp, true);
 },
 _reportStatus: function(oizImage)
 {
  if (!bizarre._Prefs.getBoolPref('showStatus'))
   return;
  var locale = document.getElementById('bizarre.stringbundle');
  var statusTextFld = document.getElementById('statusbar-display');
  var tmpStatus = 'Image Zoom: ' + oizImage.zoomFactor() + '% | ' + locale.getString('widthLabel') + ': ' + oizImage.getWidth() + 'px | ' + locale.getString('heightLabel') + ': ' + oizImage.getHeight() + 'px';
  var sFlip = '';
  switch (oizImage.getFlip())
  {
   case 'H+V':
    sFlip = ' (Flipped Horizontally and Vertically)';
    break;
   case 'H':
    sFlip = ' (Flipped Horizontally)';
    break;
   case 'V':
    sFlip = ' (Flipped Vertically)';
    break;
  }
  tmpStatus = tmpStatus + ' | ' + locale.getString('rotateLabel') + ': ' + oizImage.getAngle() + '\u00B0' + sFlip;
  statusTextFld.label = tmpStatus;
 },
 callBackStatus: function()
 {
  if (bizarre._tmpImg === null)
   return;
  bizarre._reportStatus(bizarre._tmpImg);
  bizarre._tmpImg = null;
 },
 disableContextMenu: function(evt)
 {
  if (document.popupNode.tagName.toLowerCase() === 'img' || document.popupNode.tagName.toLowerCase() === 'canvas')
  {
   bizarre._linuxImg = document.popupNode;
   bizarre._context = evt.originalTarget;
   evt.preventDefault();
   bizarre._contextDisabled = true;
  }
  removeEventListener('popupshowing', bizarre.disableContextMenu, true);
 },
 izOnMouseDown: function(evt)
 {
  if (evt.originalTarget.tagName === undefined)
   return;
  var targetName = evt.originalTarget.tagName.toLowerCase();
  if ((targetName === 'img' || targetName === 'canvas') && (bizarre._scrollZooming) && ((evt.which === bizarre._Prefs.getIntPref('imageresetbutton')) || (evt.which === bizarre._Prefs.getIntPref('imagefitbutton')) || (evt.which === bizarre._Prefs.getIntPref('triggerbutton'))))
  {
   evt.preventDefault();
   evt.stopPropagation();
  }
  if (!((evt.which === bizarre._Prefs.getIntPref('triggerbutton')) && (bizarre._Prefs.getBoolPref('usescroll')) && ((targetName === 'img' || targetName === 'canvas')) && !(targetName === 'embed' || targetName === 'object')))
   return;
  if (navigator.platform !== 'Win32' && navigator.platform !== 'Win64' && navigator.platform !== 'OS/2')
   addEventListener('popupshowing', bizarre.disableContextMenu, true);
  bizarre._haveZoomed = false;
  window.addEventListener('wheel', bizarre.handleWheelEvent, true);
  window.addEventListener('mouseup', bizarre.izOnMouseUp, true);
  window.addEventListener('click', bizarre.izOnMouseClick, true);
  bizarre._scrollZooming = true;
  bizarre._curImg = evt.originalTarget;
 },
 izOnMouseUp: function(evt)
 {
  if (evt.which !== bizarre._Prefs.getIntPref('triggerbutton'))
   return;
  if (bizarre._haveZoomed)
   evt.preventDefault();
  bizarre._cancelScrollZoom();
 },
 izOnMouseClick: function(evt)
 {
  if (evt.originalTarget === null)
   return;
  var targetName = evt.originalTarget.tagName.toLowerCase();
  if (evt.which === bizarre._Prefs.getIntPref('triggerbutton'))
  {
   if (bizarre._haveZoomed)
   {
    evt.preventDefault();
    evt.stopPropagation();
   }
   else
   {
    if (bizarre._contextDisabled)
    {
     document.popupNode = evt.originalTarget;
     try
     {
      bizarre._context.showPopup(null, evt.screenX, evt.screenY, 'context', 'bottomleft', 'topleft');
     }
     catch(e) {}
    }
   }
   bizarre._cancelScrollZoom();
   bizarre._haveZoomed = false;
   window.removeEventListener('click', bizarre.izOnMouseClick, true);
  }
  bizarre._contextDisabled = false;
  if (!bizarre._scrollZooming)
   return;
  if (targetName !== 'img' && targetName !== 'canvas')
  {
   window.removeEventListener('click', bizarre.izOnMouseClick, true);
   return;
  }
  switch (evt.which)
  {
   case bizarre._Prefs.getIntPref('imageresetbutton'):
    evt.preventDefault();
    evt.stopPropagation();
    bizarre._haveZoomed = true;
    var imgReset = new zarImage(evt.originalTarget);
    var rotateKeys = bizarre._Prefs.getIntPref('rotateKeys');
    if (rotateKeys && bizarre._modifierKeys(evt) === rotateKeys)
    {
     imgReset.resetFlip();
     imgReset.rotate(0 - imgReset.getAngle());
    }
    else
    {
     if (bizarre._Prefs.getBoolPref('toggleFitReset') && imgReset.zoomFactor() === 100)
      imgReset.fit(bizarre._Prefs.getBoolPref('autocenter'));
     else if(imgReset.zoomFactor() !== 100)
      imgReset.setZoom(100);
     else if(targetName === 'img')
     {
      var img = evt.originalTarget;
      imgReset.setDimension(img.naturalWidth, img.naturalHeight);
     }
    }
    bizarre._reportStatus(imgReset);
    break;
   case bizarre._Prefs.getIntPref('imagefitbutton'):
    evt.preventDefault();
    evt.stopPropagation();
    bizarre._haveZoomed = true;
    var imgFit = new zarImage(evt.originalTarget);
    if (bizarre._Prefs.getBoolPref('toggleFitReset') && imgFit.isFitted())
     imgFit.setZoom(100);
    else
     imgFit.fit(bizarre._Prefs.getBoolPref('autocenter'));
    bizarre._reportStatus(imgFit);
    break;
  }
 },
 handleWheelEvent: function(evt)
 {
  if (!((bizarre._scrollZooming) && (bizarre._curImg) && (bizarre._Prefs.getBoolPref('usescroll'))))
   return;
  evt.preventDefault();
  var scrollAmount;
  if (Math.abs(evt.deltaY) > Math.abs(evt.deltaX))
   scrollAmount = evt.deltaY;
  else
   scrollAmount = evt.deltaX;
  bizarre.scrollImage(scrollAmount, evt);
 },
 _modifierKeys: function(evt)
 {
  return evt.ctrlKey + evt.altKey * 2 + evt.shiftKey * 4;
 },
 scrollImage: function(wheelIncrement, evt)
 {
  var imgToScroll = bizarre._curImg;
  if (imgToScroll === null)
  {
   bizarre._cancelScrollZoom();
   return;
  }
  bizarre._haveZoomed = true;
  var oizImage = new zarImage(imgToScroll);
  var rotateKeys = bizarre._Prefs.getIntPref('rotateKeys');
  if (rotateKeys && bizarre._modifierKeys(evt) === rotateKeys)
  {
   oizImage.rotate(bizarre._Prefs.getIntPref('rotateValue') * (wheelIncrement > 0 ? 1 : -1));
   return;
  }
  var zoomFactor;
  if (bizarre._Prefs.getIntPref('scrollmode') === 0)
  {
   if (((wheelIncrement < 0) && !bizarre._Prefs.getBoolPref('reversescrollzoom')) || ((wheelIncrement > 0) && bizarre._Prefs.getBoolPref('reversescrollzoom')))
    zoomFactor = 1 / (1 + (bizarre._Prefs.getIntPref('scrollvalue') / 100));
   else
    zoomFactor = 1 + (bizarre._Prefs.getIntPref('scrollvalue') / 100);
   oizImage.zoom(zoomFactor);
  }
  else
  {
   if (((wheelIncrement < 0) && !bizarre._Prefs.getBoolPref('reversescrollzoom')) || ((wheelIncrement > 0) && bizarre._Prefs.getBoolPref('reversescrollzoom')))
    zoomFactor = oizImage.zoomFactor() - bizarre._Prefs.getIntPref('scrollvalue');
   else
    zoomFactor = oizImage.zoomFactor() + bizarre._Prefs.getIntPref('scrollvalue');
   oizImage.setZoom(zoomFactor);
  }
  bizarre._reportStatus(oizImage);
 },
 _insertSeparator: function(list, position)
 {
  var i;
  for (i = position - 1; i >= 0; i--)
  {
   if(list[i].hidden)
    continue;
   if (list[i].tagName === 'menuseparator')
    return false;
   break;
  }
  for (i = position + 1; i < list.length; i++)
  {
   if(list[i].hidden)
    continue;
   if (list[i].tagName === 'menuseparator')
    return false;
   return true;
  }
  return false;
 },
 showMenu: function()
 {
  if (gContextMenu === null)
   return;
  var menuItems = new Array('context-zoom-zin', 'context-zoom-zout', 'context-zoom-zreset', 'context-zoom-zcustom', 'context-zoom-dcustom', 'context-zoom-fit', 'context-zoom-fitwidth', 'context-zoom-rotate-right', 'context-zoom-rotate-left', 'context-zoom-rotate-180', 'context-zoom-flip-h', 'context-zoom-flip-v', 'context-zoom-rotate-reset', 'zoomsub-zin', 'zoomsub-zout', 'zoomsub-zreset', 'rotatesub-rotate-right', 'rotatesub-rotate-left', 'rotatesub-rotate-180', 'rotatesub-flip-h', 'rotatesub-flip-v', 'rotatesub-rotate-reset', 'zoomsub-zcustom', 'zoomsub-dcustom', 'zoomsub-fit', 'zoomsub-fitwidth', 'zoomsub-z400', 'zoomsub-z200', 'zoomsub-z150', 'zoomsub-z125', 'zoomsub-z100', 'zoomsub-z75', 'zoomsub-z50', 'zoomsub-z25', 'zoomsub-z10');
  var optionItems = new Array('mmZoomIO', 'mmZoomIO', 'mmReset', 'mmCustomZoom', 'mmCustomDim', 'mmFitWindow', 'mmFitWidth', 'mmRotateRight', 'mmRotateLeft', 'mmRotate180', 'mmFlipH', 'mmFlipV', 'mmRotateReset', 'smZoomIO', 'smZoomIO', 'smReset', 'smRotateRight', 'smRotateLeft', 'smRotate180', 'smFlipH', 'smFlipV', 'smRotateReset', 'smCustomZoom', 'smCustomDim', 'smFitWindow', 'smFitWidth', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts', 'smZoomPcts');
  var menuHide = new Array();
  var oizImage = null;
  if (gContextMenu.onImage || gContextMenu.onCanvas)
   oizImage = new zarImage(document.popupNode, true);
  if (oizImage !== null)
  {
   if (oizImage.inImageViewer() === 3)
   {
    menuItems = new Array();
    optionItems = new Array();
    menuHide = new Array('context-zoom-zin', 'context-zoom-zout', 'context-zoom-zreset', 'context-zoom-zcustom', 'context-zoom-dcustom', 'context-zoom-fit', 'context-zoom-fitwidth', 'context-zoom-rotate-right', 'context-zoom-rotate-left', 'context-zoom-rotate-180', 'context-zoom-flip-h', 'context-zoom-flip-v', 'context-zoom-rotate-reset', 'zoomsub-zin', 'zoomsub-zout', 'zoomsub-zreset', 'rotatesub-rotate-right', 'rotatesub-rotate-left', 'rotatesub-rotate-180', 'rotatesub-flip-h', 'rotatesub-flip-v', 'rotatesub-rotate-reset', 'zoomsub-zcustom', 'zoomsub-dcustom', 'zoomsub-fit', 'zoomsub-fitwidth', 'zoomsub-z400', 'zoomsub-z200', 'zoomsub-z150', 'zoomsub-z125', 'zoomsub-z100', 'zoomsub-z75', 'zoomsub-z50', 'zoomsub-z25', 'zoomsub-z10');    
   }
   else if (oizImage.inImageViewer() === 2)
   {
    menuItems = new Array('context-zoom-rotate-right', 'context-zoom-rotate-left', 'context-zoom-rotate-180', 'context-zoom-flip-h', 'context-zoom-flip-v', 'context-zoom-rotate-reset', 'rotatesub-rotate-right', 'rotatesub-rotate-left', 'rotatesub-rotate-180', 'rotatesub-flip-h', 'rotatesub-flip-v', 'rotatesub-rotate-reset');
    optionItems = new Array('mmRotateRight', 'mmRotateLeft', 'mmRotate180', 'mmFlipH', 'mmFlipV', 'mmRotateReset', 'smRotateRight', 'smRotateLeft', 'smRotate180', 'smFlipH', 'smFlipV', 'smRotateReset');
    menuHide = new Array('context-zoom-zin', 'context-zoom-zout', 'context-zoom-zreset', 'context-zoom-zcustom', 'context-zoom-dcustom', 'context-zoom-fit', 'context-zoom-fitwidth', 'zoomsub-zin', 'zoomsub-zout', 'zoomsub-zreset', 'zoomsub-zcustom', 'zoomsub-dcustom', 'zoomsub-fit', 'zoomsub-fitwidth', 'zoomsub-z400', 'zoomsub-z200', 'zoomsub-z150', 'zoomsub-z125', 'zoomsub-z100', 'zoomsub-z75', 'zoomsub-z50', 'zoomsub-z25', 'zoomsub-z10');
   }
  }
  var i;
  for (i = 0; i < menuHide.length; i++)
  {
   document.getElementById(menuHide[i]).setAttribute('hidden', true);
  }
  for (i = 0; i < menuItems.length; i++)
  {
   document.getElementById(menuItems[i]).setAttribute('hidden', (oizImage === null || !bizarre._Prefs.getBoolPref(optionItems[i])).toString());
  }
  var subPopUp = document.getElementById('zoompopup');
  var subItems = subPopUp.getElementsByTagName('*');
  for (i = 0; i < subItems.length; i++)
  {
   if (subItems[i].tagName === 'menuseparator')
    subItems[i].setAttribute('hidden', (!bizarre._insertSeparator(subItems, i)).toString());
  }
  if (subPopUp.getElementsByAttribute('hidden', false).length > 0)
  {
   var mnuZoom = document.getElementById('context-zoomsub');
   var sZoom = '100';
   if (oizImage !== null)
    sZoom = '' + oizImage.zoomFactor();
   mnuZoom.setAttribute('label', bizarre._getContextSubMenuLabel().replace(/%zoom%/, sZoom));
   mnuZoom.setAttribute('hidden', 'false');
  }
  else
   document.getElementById('context-zoomsub').hidden = true;
  var rotatePopUp = document.getElementById('rotatepopup');
  if (rotatePopUp.getElementsByAttribute('hidden', false).length > 0)
  {
   var mnuRotate = document.getElementById('context-rotatesub');
   var sAngle = '0';
   if (oizImage !== null)
    sAngle = '' + oizImage.getAngle();
   mnuRotate.setAttribute('label', bizarre._getContextRotateMenuLabel().replace(/%rotate%/, sAngle));
   mnuRotate.setAttribute('hidden', 'false');
  }
  else
   document.getElementById('context-rotatesub').hidden = true;
 }
};

function zarImage(oImage, resetDimensions)
{
 var pImage = oImage;
 var enabled = false;
 if ((pImage.naturalWidth !== 0) || (pImage.naturalHeight !== 0) || (pImage.style.width !== '') || (pImage.style.height !== '') || ((pImage.getAttribute('width')) && (pImage.getAttribute('height'))))
 {
  if (pImage.originalStyle === undefined)
  {
   if (pImage.hasAttribute('style'))
    pImage.originalStyle = pImage.getAttribute('style');
   else
    pImage.originalStyle = null;
   pImage.originalPxWidth = pImage.width;
   pImage.originalPxHeight = pImage.height;
   pImage.zoomFactor = 100;
   pImage.angle = 0;
   pImage.hFlip = false;
   pImage.vFlip = false;
   pImage.ignoreSrc = false;
   pImage.ignoreStyles = [];
  }
  else if (resetDimensions && pImage.zoomFactor === 100 && pImage.angle === 0)
  {
   pImage.originalPxWidth = pImage.width;
   pImage.originalPxHeight = pImage.height;
  }
  enabled = true;
 }
 if (pImage.observer === undefined)
 {
  pImage.observer = new MutationObserver(
   function(mutations, observer)
   {
    for (m of mutations)
    {
     if (m.type.toLowerCase() !== 'attributes')
      continue;
     var attr = m.attributeName.toLowerCase();
     var val = pImage.getAttribute(m.attributeName);
     switch (attr)
     {
      case 'src':
       changedSrc(val, m.oldValue);
       break;
      case 'style':
       changedStyle(val, m.oldValue);
       break;
      case 'id':
      case 'class':
      case 'width':
      case 'height':
       changedGeneric(val, m.oldValue);
       
     }
    }
   }
  );
  pImage.observer.observe(pImage, {attributeOldValue: true});
 }
 this.getWidth = getWidth;
 this.getAngle = getAngle;
 this.getFlip = getFlip;
 this.resetFlip = resetFlip;
 this.getHeight = getHeight;
 this.setZoom = setZoom;
 this.setDimension = setDimension;
 this.zoom = zoom;
 this.fit = fit;
 this.rotate = rotate;
 this.zoomFactor = zoomFactor;
 this.isFitted = isFitted;
 this.inImageViewer = inImageViewer;
 function changedGeneric(val, oldVal)
 {
  if (val === oldVal)
   return;
  if (inImageViewer() !== 0)
   return;
  pImage.zoomFactor = 100;
  pImage.angle = 0;
  resetFlip();
  unsetDimension();
  if (pImage.originalSrc && pImage.src !== pImage.originalSrc)
   pImage.src = pImage.originalSrc;
 }
 function changedSrc(val, oldVal)
 {
  if (val === oldVal)
   return;
  if (val === pImage.ignoreSrc)
  {
   pImage.ignoreSrc = false;
   return;
  }
  pImage.originalSrc = val;
  pImage.zoomFactor = 100;
  pImage.angle = 0;
  resetFlip();
  unsetDimension();
  pImage.ignoreSrc = false;
 }
 function changedStyle(val, oldVal)
 {
  if (inImageViewer() !== 0)
   return;
  var oldStyle = parseStyle(oldVal);
  var newStyle = parseStyle(val);
  var baseStyle = [];
  if (pImage.originalStyle !== null)
   baseStyle = parseStyle(pImage.originalStyle);
  var s, o, g;
  var c = false;
  for (s in newStyle)
  {
   var ignore = false;
   for (g in pImage.ignoreStyles)
   {
    if (s === g)
    {
     if (newStyle[s] === pImage.ignoreStyles[g])
     {
      ignore = true;
      break;
     }
     delete pImage.ignoreStyles[g];
    }
   }
   if (ignore)
    continue;
   for (o in oldStyle)
   {
    if (s === o && newStyle[s] === oldStyle[o])
    {
     ignore = true;
     break;
    }
   }
   if (ignore)
    continue;
   baseStyle[s] = newStyle[s];
   switch (s)
   {
    case 'min-width':
    case 'width':
    case 'max-width':
    case 'min-height':
    case 'height':
    case 'max-height':
     c = true;
     break;
   }
  }
  pImage.originalStyle = '';
  var b;
  for (b in baseStyle)
  {
   pImage.originalStyle += b + ': ' + baseStyle[b] + ';';
  }
  if (!c)
   return;
  pImage.zoomFactor = 100;
  pImage.angle = 0;
  delete pImage.ignoreStyles;
  pImage.ignoreStyles = [];
  for (b in baseStyle)
  {
   pImage.ignoreStyles[b] = baseStyle[b];
  }
  resetFlip();
  unsetDimension();
  if (pImage.originalSrc && pImage.src !== pImage.originalSrc)
   pImage.src = pImage.originalSrc;
 }
 function getWidth()
 {
  return pImage.width;
 }
 function getHeight()
 {
  return pImage.height;
 }
 function getAngle()
 {
  return pImage.angle;
 }
 function getFlip()
 {
  if (pImage.hFlip === true && pImage.vFlip === true)
   return 'H+V';
  if (pImage.hFlip === true)
   return 'H';
  if (pImage.vFlip === true)
   return 'V';
  return '';
 }
 function resetFlip()
 {
  pImage.hFlip = false;
  pImage.vFlip = false;
  let iStyle = null;
  if (pImage.hasAttribute('style'))
   iStyle = pImage.getAttribute('style');
  if (pImage.originalStyle === iStyle)
   delete pImage.originalData;
 }
 function setZoom(factor)
 {
  if (!((factor > 0) && (enabled)))
   return;
  pImage.zoomFactor = factor;
  pZoomAbs();
 }
 function unsetDimension()
 {
  if (pImage.originalStyle === null)
   pImage.removeAttribute('style');
  else
   pImage.setAttribute('style', pImage.originalStyle);
  if (pImage.hFlip === false && pImage.vFlip === false)
   delete pImage.originalData;
 }
 function inImageViewer()
 {
  if (Object.getPrototypeOf(pImage.ownerDocument).toString() !== ImageDocument.prototype.toString())
   return 0;
  if (pImage.ownerDocument.imageIsResized)
   return 2;
  if (pImage.ownerDocument.imageIsOverflowing)
   return 3;
  return 1;
 }
 function setRawDimension(width, height)
 {
  var w = Math.floor(width) + 'px';
  var h = Math.floor(height) + 'px';
  pImage.ignoreStyles['min-width'] = w;
  pImage.ignoreStyles['width'] = w;
  pImage.ignoreStyles['max-width'] = w;
  pImage.ignoreStyles['min-height'] = h;
  pImage.ignoreStyles['height'] = h;
  pImage.ignoreStyles['max-height'] = h;
  pImage.style.minWidth = w;
  pImage.style.width = w;
  pImage.style.maxWidth = w;
  pImage.style.minHeight = h;
  pImage.style.height = h;
  pImage.style.maxHeight = h;
 }
 function parseStyle(s)
 {
  var regex = /([\w-]*)\s*:\s*([^;]*)/g;
  var match, properties={};
  while(match=regex.exec(s))
   properties[match[1].toLowerCase()] = match[2].trim();
  return properties;
 }
 function setDimension(width, height)
 {
  if (!enabled)
   return;
  pImage.zoomFactor = (width / pImage.originalPxWidth) * 100;
  if (pImage.zoomFactor === 100 && pImage.angle === 0)
   unsetDimension();
  else
   setRawDimension(width, height);
 }
 function setDefaultDimension(useOD)
 {
  if (!enabled)
   return;
  var oImage = pImage;
  if (useOD && pImage.originalData !== undefined)
   oImage = pImage.originalData;
  var w = oImage.originalPxWidth * (pImage.zoomFactor / 100);
  var h = oImage.originalPxHeight * (pImage.zoomFactor / 100);
  setRawDimension(w, h);
 }
 function zoom(factor)
 {
  if (!enabled)
   return;
  pImage.zoomFactor = pImage.zoomFactor * factor;
  pZoomAbs();
 }
 function rotate(degrees, direction)
 {
  if (new Date().getTime() - bizarre.rotateTime < 200)
   return;
  bizarre.rotateTime = new Date().getTime();
  if (!pImage.originalSrc)
  {
   if (pImage.tagName.toLowerCase() === 'canvas')
    pImage.originalSrc = pImage.getDataUrl();
   else
    pImage.originalSrc = pImage.src;
  }
  if (!pImage.originalData)
  {
   pImage.originalData = {
    naturalWidth: pImage.naturalWidth,
    naturalHeight: pImage.naturalHeight,
    originalPxWidth: pImage.originalPxWidth,
    originalPxHeight: pImage.originalPxHeight
   };
  }
  var origSrc = pImage.originalSrc;
  var origData = pImage.originalData;
  if (typeof degrees === 'undefined')
   degrees = 0;
  if (degrees < 0)
   degrees = (pImage.angle + 360 + (degrees % 360)) % 360;
  else
   degrees = (pImage.angle + degrees) % 360;
  if (typeof direction !== 'undefined')
  {
   if (direction === 'h')
    pImage.hFlip = !pImage.hFlip;
   else
    pImage.vFlip = !pImage.vFlip;
  }
  if (degrees === 0 && pImage.hFlip === false && pImage.vFlip === false)
  {
   pImage.angle = degrees;
   if (inImageViewer() === 0)
   {
    pImage.ignoreSrc = origSrc;
    pImage.src = origSrc;
   }
   else
   {
    pImage.removeAttribute('srcset');
    pImage.removeAttribute('sizes');
   }
   pImage.originalPxWidth = origData.originalPxWidth;
   pImage.originalPxHeight = origData.originalPxHeight;
   if (pImage.zoomFactor === 100)
    unsetDimension();
   else
    setDefaultDimension(true);
   bizarre.callBackStatus();
   return;
  }
  var theta;
  if (degrees >= 0)
   theta = (Math.PI * degrees) / 180;
  else
   theta = (Math.PI * (360 + degrees)) / 180;
  var costheta = Math.abs(Math.cos(theta));
  var sintheta = Math.abs(Math.sin(theta));
  var canvas = pImage.ownerDocument.createElementNS('http://www.w3.org/1999/xhtml', 'canvas');
  canvas.width = costheta * origData.naturalWidth + sintheta * origData.naturalHeight;
  canvas.height = costheta * origData.naturalHeight + sintheta * origData.naturalWidth;
  canvas.oImage = new Image();
  canvas.oImage.src = origSrc;
  canvas.oImage.onload = function()
  {
   var ctx = canvas.getContext('2d');
   ctx.save();
   if (pImage.hFlip === true && pImage.vFlip === true)
   {
    ctx.translate(canvas.width, canvas.height);
    ctx.scale(-1, -1);
   }
   else if (pImage.hFlip === true)
   {
    ctx.translate(canvas.width, 0);
    ctx.scale(-1, 1);
   }
   else if (pImage.vFlip === true)
   {
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
   }
   if (theta <= Math.PI / 2)
    ctx.translate(Math.sin(theta) * canvas.oImage.naturalHeight, 0);
   else if (theta <= Math.PI)
    ctx.translate(canvas.width, -Math.cos(theta) * canvas.oImage.naturalHeight);
   else if (theta <= 1.5 * Math.PI)
    ctx.translate(-Math.cos(theta) * canvas.oImage.naturalWidth, canvas.height);
   else
    ctx.translate(0, -Math.sin(theta) * canvas.oImage.naturalWidth);
   pImage.originalPxWidth = costheta * origData.originalPxWidth + sintheta * origData.originalPxHeight;
   pImage.originalPxHeight = costheta * origData.originalPxHeight + sintheta * origData.originalPxWidth;
   if (pImage.zoomFactor === 100 && degrees === 0)
    unsetDimension();
   else
   {
    if (inImageViewer() > 1)
    {
     if (pImage.originalStyle === null)
      pImage.removeAttribute('style');
     else
      pImage.setAttribute('style', pImage.originalStyle);
    }
    else
    {
     var t0 = Math.PI * pImage.angle / 180;
     var natWidthT0 = Math.abs(Math.cos(t0)) * origData.naturalWidth + Math.abs(Math.sin(t0)) * origData.naturalHeight;
     var natHeightT0 = Math.abs(Math.cos(t0)) * origData.naturalHeight + Math.abs(Math.sin(t0)) * origData.naturalWidth;
     var w = pImage.width * canvas.width / natWidthT0;
     var h = pImage.height * canvas.height / natHeightT0;
     setRawDimension(w, h);
    }
   }
   pImage.angle = degrees;
   ctx.rotate(theta);
   ctx.clearRect(0, 0, canvas.oImage.naturalWidth, canvas.oImage.naturalHeight);
   ctx.drawImage(canvas.oImage, 0, 0, canvas.oImage.naturalWidth, canvas.oImage.naturalHeight);
   ctx.restore();
   var dURL = canvas.toDataURL();
   if (inImageViewer() === 0)
   {
    pImage.ignoreSrc = dURL;
    pImage.src = dURL;
   }
   else
   {
    pImage.setAttribute('srcset', dURL + ' 1w');
    pImage.setAttribute('sizes', '1px');
   }
   bizarre.callBackStatus();
  };
 }
 function isFitted()
 {
  var imageDiff;
  var bScreen = new zarScreen(pImage);
  var screenHeight = bScreen.getHeight();
  var screenWidth = bScreen.getWidth();
  var screenDim = screenWidth / screenHeight;
  var imageDim = pImage.width / pImage.height;
  if (screenDim < imageDim)
   imageDiff = Math.abs(screenWidth - pImage.width);
  else
   imageDiff = Math.abs(screenHeight - pImage.height);
  return imageDiff < 50;
 }
 function fit(autoScroll, widthOnly)
 {
  if (!enabled)
   return;
  var bScreen = new zarScreen(pImage);
  var screenHeight = bScreen.getHeight();
  var screenWidth = bScreen.getWidth();
  var screenDim = screenWidth / screenHeight;
  var imageDim = pImage.width / pImage.height;
  if (screenDim < imageDim || widthOnly)
   setDimension(screenWidth, parseInt(screenWidth / imageDim + 0.5, 10));
  else
   setDimension(parseInt(screenHeight * imageDim + 0.5, 10), screenHeight);
  screenHeight = bScreen.getHeight();
  screenWidth = bScreen.getWidth();
  if (screenDim < imageDim || widthOnly)
   setDimension(screenWidth, parseInt(screenWidth / imageDim + 0.5,10));
  else 
   setDimension(parseInt(screenHeight * imageDim + 0.5,10), screenHeight);
  if (!autoScroll)
   return;
  var iTop = 0;
  var iLeft = 0;
  var cNode = pImage;
  while (cNode.tagName.toUpperCase() !== 'BODY')
  {
   iLeft += cNode.offsetLeft;
   iTop += cNode.offsetTop;
   cNode = cNode.offsetParent;
  }
  if (screenDim < imageDim || widthOnly)
   pImage.ownerDocument.defaultView.scroll(iLeft - (bScreen.getPad()), iTop - ((screenHeight - pImage.height) / 2) - (bScreen.getPad()));
  else 
   pImage.ownerDocument.defaultView.scroll(iLeft - ((screenWidth - pImage.width) / 2) - (bScreen.getPad()), iTop - (bScreen.getPad()));
 }
 function zoomFactor()
 {
  return parseInt(parseInt(pImage.zoomFactor,10) + 0.5,10);
 }
 function pZoomAbs()
 {
  pImage.ignoreStyles['margin-top'] = '';
  pImage.style.marginTop = '';
  if (pImage.zoomFactor === 100 && pImage.angle === 0)
  {
   unsetDimension();
   return;
  }
  setDefaultDimension();
  if (pImage.y < 0 || pImage.offsetTop < 0)
  {
   pImage.ignoreStyles['margin-top'] = '0';
   pImage.style.marginTop = '0';
  }
  else
  {
   pImage.ignoreStyles['margin-top'] = '';
   pImage.style.marginTop = '';
  }
 }
}

function zarScreen(pImage)
{
 const padValue = 17;
 this.getWidth = function()
 {
  if (pImage.ownerDocument.compatMode === 'BackCompat')
   return pImage.ownerDocument.body.clientWidth - padValue;
  return pImage.ownerDocument.documentElement.clientWidth - padValue;
 };
 this.getHeight = function()
 {
  if (pImage.ownerDocument.compatMode === 'BackCompat')
   return pImage.ownerDocument.body.clientHeight - padValue;
  return pImage.ownerDocument.documentElement.clientHeight - padValue;
 };
 this.getPad = function()
 {
  return padValue / 2;
 };
}

window.addEventListener('load', bizarre.init, false);
